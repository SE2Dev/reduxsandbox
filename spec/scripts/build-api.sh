#!/bin/sh

# IDK it told me to do it
export TS_POST_PROCESS_FILE="/usr/local/bin/prettier --write"

# Specification file
OPENAPI_SPEC_FILE="spec/typicode.yaml"

# Resolve the specification name from the file we chose
# See https://stackoverflow.com/a/14703709 for information on the expansion
# First we strip everything after the first .
OPENAPI_SPEC_NAME="${OPENAPI_SPEC_FILE##*/}"
# Then we strip everything after the last .
OPENAPI_SPEC_NAME="${OPENAPI_SPEC_NAME%%.*}"

# Output directory
OPENAPI_API_DIR="src/api/$OPENAPI_SPEC_NAME"

# Generator we want to use
OPENAPI_GENERATOR_NAME="typescript-fetch"

# Generator config file
OPENAPI_GENERATOR_CONFIG="spec/config/$OPENAPI_GENERATOR_NAME.json"

# Generate the actual API
openapi-generator-cli generate					\
	--generator-name $OPENAPI_GENERATOR_NAME	\
	--config $OPENAPI_GENERATOR_CONFIG			\
	--input-spec $OPENAPI_SPEC_FILE				\
	--output $OPENAPI_API_DIR