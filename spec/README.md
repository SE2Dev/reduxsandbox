# /spec
This directory houses our OpenAPI specification files. 
Which should then be used to generate the /src/api/[apiName] directories.

Note that config.json is used to configure the API generator properties.

An example script to generate the actual api files would be:
```bash
#!/bin/sh

# IDK it told me to do it
export TS_POST_PROCESS_FILE="/usr/local/bin/prettier --write"

openapi-generator-cli generate --config spec/config/typescript-fetch.json -i spec/typicode.yaml --generator-name typescript-fetch -o src/api/typicode
```