tags:
  -
    name: Posts
  -
    name: Comments
  -
    name: Albums
  -
    name: Photos
  -
    name: Todos
  -
    name: Users
paths:
  /posts:
    get:
      operationId: getPosts
      summary: 'Get a list of posts'
      description: 'Returns all posts from the system that the user has access to.'
      tags:
        - Posts
      responses:
        '200':
          description: 'A list of posts.'
          schema:
            x-oad-type: array
            type: array
            items:
              x-oad-type: reference
              $ref: '#/definitions/Post'
          x-oad-type: response
      parameters:
        -
          name: userId
          in: query
          description: 'Only get the posts for the specified user.'
          required: false
          type: integer
          format: int32
          x-oad-type: parameter
    x-oad-type: operation
  /comments:
    get:
      operationId: getComments
      summary: 'Get a list of comments'
      description: 'Returns all comments from the system that the user has access to.'
      tags:
        - Comments
      responses:
        '200':
          description: 'A list of comments.'
          schema:
            x-oad-type: array
            type: array
            items:
              x-oad-type: reference
              $ref: '#/definitions/Comment'
          x-oad-type: response
      parameters:
        -
          name: postId
          in: query
          description: 'Only get the comments for the specified post'
          required: false
          type: integer
          format: int32
          x-oad-type: parameter
    x-oad-type: operation
  /albums:
    get:
      operationId: getAlbums
      summary: 'Get a list of albums'
      description: 'Returns all albums from the system that the user has access to.'
      tags:
        - Albums
      responses:
        '200':
          description: 'A list of albums.'
          schema:
            x-oad-type: array
            type: array
            items:
              x-oad-type: reference
              $ref: '#/definitions/Album'
          x-oad-type: response
      parameters:
        -
          name: userId
          in: query
          description: 'Only get albums for the specified user'
          required: false
          type: integer
          format: int32
          x-oad-type: parameter
    x-oad-type: operation
  /photos:
    get:
      operationId: getPhotos
      summary: 'Get a list of photos'
      description: 'Returns all photos from the system that the user has access to.'
      tags:
        - Photos
      responses:
        '200':
          description: 'A list of photos.'
          schema:
            x-oad-type: array
            type: array
            items:
              x-oad-type: reference
              $ref: '#/definitions/Photo'
          x-oad-type: response
      parameters:
        -
          name: albumId
          in: path
          description: 'Only get photos for the specified album'
          required: true
          type: integer
          format: int32
          x-oad-type: parameter
    x-oad-type: operation
  /todos:
    get:
      operationId: getTodos
      summary: 'Get a list of todos.'
      description: 'Returns all todos from the system that the user has access to.'
      tags:
        - Todos
      responses:
        '200':
          description: 'A list of todos.'
          schema:
            x-oad-type: array
            type: array
            items:
              x-oad-type: reference
              $ref: '#/definitions/Todo'
          x-oad-type: response
      parameters:
        -
          name: userId
          in: path
          description: 'Only get todos for the specified user'
          required: true
          type: string
          x-oad-type: parameter
    x-oad-type: operation
  /users:
    get:
      operationId: getUsers
      summary: 'Get a list of users'
      description: 'Returns all users from the system that the user has access to.'
      tags:
        - Users
      responses:
        '200':
          description: 'A list of users.'
          schema:
            x-oad-type: array
            type: array
            items:
              x-oad-type: reference
              $ref: '#/definitions/User'
          x-oad-type: response
    x-oad-type: operation
  '/posts/{postId}':
    get:
      operationId: getPost
      summary: 'Get a specific post by id'
      tags:
        - Posts
      responses:
        '200':
          description: 'A post'
          schema:
            x-oad-type: reference
            $ref: '#/definitions/Post'
          x-oad-type: response
      parameters:
        -
          name: postId
          in: path
          required: true
          type: integer
          format: int32
          x-oad-type: parameter
    x-oad-type: operation
  '/posts/{postId}/comments':
    get:
      operationId: getCommentsForPost
      summary: 'Get all comments for a specific post'
      tags:
        - Posts
        - Comments
      responses:
        '200':
          description: 'All comments for the specified post'
          schema:
            x-oad-type: array
            type: array
            items:
              x-oad-type: reference
              $ref: '#/definitions/Comment'
          x-oad-type: response
      parameters:
        -
          name: postId
          in: path
          required: true
          type: integer
          format: int32
          x-oad-type: parameter
    x-oad-type: operation
  '/comments/{commentId}':
    get:
      operationId: getComment
      summary: 'Get a specific comment by id'
      tags:
        - Comments
      responses:
        '200':
          description: 'A comment response'
          schema:
            x-oad-type: reference
            $ref: '#/definitions/Comment'
          x-oad-type: response
      parameters:
        -
          name: commentId
          in: path
          required: true
          type: integer
          format: int32
          x-oad-type: parameter
    x-oad-type: operation
  '/albums/{albumId}':
    get:
      operationId: getAlbum
      summary: 'Get a specific album by id'
      tags:
        - Albums
      responses:
        '200':
          description: 'An album response'
          schema:
            x-oad-type: reference
            $ref: '#/definitions/Album'
          x-oad-type: response
      parameters:
        -
          name: albumId
          in: path
          required: true
          type: integer
          format: int32
          x-oad-type: parameter
    x-oad-type: operation
  '/photos/{photoId}':
    get:
      operationId: getPhoto
      summary: 'Get a specific photo by id'
      tags:
        - Photos
      responses:
        '200':
          description: 'A photo response'
          schema:
            x-oad-type: reference
            $ref: '#/definitions/Photo'
          x-oad-type: response
      parameters:
        -
          name: photoId
          in: path
          required: true
          type: integer
          format: int32
          x-oad-type: parameter
    x-oad-type: operation
  '/todos/{todoId}':
    get:
      operationId: getTodo
      summary: 'Get a specific todo by id'
      tags:
        - Todos
      responses:
        '200':
          description: 'A todo response'
          schema:
            x-oad-type: reference
            $ref: '#/definitions/Todo'
          x-oad-type: response
      parameters:
        -
          name: todoId
          in: path
          required: true
          type: integer
          format: int32
          x-oad-type: parameter
    x-oad-type: operation
  '/users/{userId}':
    get:
      operationId: getUser
      summary: 'Get a specific user by id'
      tags:
        - Users
      responses:
        '200':
          description: 'A user response'
          schema:
            x-oad-type: reference
            $ref: '#/definitions/User'
          x-oad-type: response
      parameters:
        -
          name: userId
          in: path
          description: "DONT USE REFERENCES TO PARAMETER TYPES\nIT BREAKS OpenAPI DESIGNER"
          required: true
          type: integer
          format: int32
          x-oad-type: parameter
    x-oad-type: operation
  '/albums/{albumId}/photos':
    get:
      operationId: getPhotosForAlbum
      summary: 'Get all photos for the specified album'
      tags:
        - Albums
        - Photos
      responses:
        '200':
          description: 'A list of photos'
          schema:
            x-oad-type: array
            type: array
            items:
              x-oad-type: reference
              $ref: '#/definitions/Photo'
          x-oad-type: response
      parameters:
        -
          name: albumId
          in: path
          required: true
          type: integer
          format: int32
          x-oad-type: parameter
    x-oad-type: operation
  '/users/{userId}/albums':
    get:
      operationId: getAlbumsForUser
      summary: 'Get all albums for the specified user'
      tags:
        - Users
        - Albums
      responses:
        '200':
          description: 'A list of albums'
          schema:
            x-oad-type: array
            type: array
            items:
              x-oad-type: reference
              $ref: '#/definitions/User'
          x-oad-type: response
      parameters:
        -
          name: userId
          in: path
          required: true
          type: integer
          format: int32
          x-oad-type: parameter
    x-oad-type: operation
  '/users/{userId}/todos':
    get:
      operationId: getTodosForUser
      summary: 'Get all todos for a specific user'
      tags:
        - Users
        - Todos
      responses:
        '200':
          description: 'A list of todos'
          schema:
            x-oad-type: array
            type: array
            items:
              x-oad-type: reference
              $ref: '#/definitions/Todo'
          x-oad-type: response
      parameters:
        -
          name: userId
          in: path
          required: true
          type: integer
          format: int32
          x-oad-type: parameter
    x-oad-type: operation
  '/users/{userId}/posts':
    get:
      operationId: getPostsForUser
      summary: 'Get all posts for the specified user'
      tags:
        - Users
        - Posts
      responses:
        '200':
          description: 'A list of posts'
          schema:
            x-oad-type: array
            type: array
            items:
              x-oad-type: reference
              $ref: '#/definitions/Post'
          x-oad-type: response
      parameters:
        -
          name: userId
          in: path
          required: true
          type: integer
          format: int32
          x-oad-type: parameter
    x-oad-type: operation
definitions:
  Post:
    x-oad-type: object
    type: object
    properties:
      userId:
        x-oad-type: integer
        type: integer
        format: int32
      id:
        x-oad-type: integer
        type: integer
        format: int32
      title:
        x-oad-type: string
        type: string
      body:
        x-oad-type: string
        type: string
    required:
      - id
  Comment:
    x-oad-type: object
    type: object
    properties:
      postId:
        x-oad-type: integer
        type: integer
        format: int32
      id:
        x-oad-type: integer
        type: integer
        format: int32
      name:
        x-oad-type: string
        type: string
      email:
        x-oad-type: string
        type: string
      body:
        x-oad-type: string
        type: string
    required:
      - id
  Album:
    x-oad-type: object
    type: object
    properties:
      userId:
        x-oad-type: integer
        type: integer
        format: int32
      id:
        x-oad-type: integer
        type: integer
        format: int32
      title:
        x-oad-type: string
        type: string
    required:
      - id
  Photo:
    x-oad-type: object
    type: object
    properties:
      albumId:
        x-oad-type: integer
        type: integer
        format: int32
      id:
        x-oad-type: integer
        type: integer
        format: int32
      title:
        x-oad-type: string
        type: string
      url:
        x-oad-type: string
        type: string
      thumbnailUrl:
        x-oad-type: string
        type: string
    required:
      - id
  Todo:
    x-oad-type: object
    type: object
    properties:
      userId:
        x-oad-type: integer
        type: integer
        format: int32
      id:
        x-oad-type: integer
        type: integer
        format: int32
      title:
        x-oad-type: string
        type: string
      completed:
        x-oad-type: boolean
        type: boolean
    required:
      - id
  User:
    x-oad-type: object
    type: object
    properties:
      id:
        x-oad-type: integer
        type: integer
        format: int32
      name:
        x-oad-type: string
        type: string
      username:
        x-oad-type: string
        type: string
      email:
        x-oad-type: string
        type: string
      address:
        x-oad-type: object
        type: object
        properties:
          street:
            x-oad-type: string
            type: string
          suite:
            x-oad-type: string
            type: string
          city:
            x-oad-type: string
            type: string
          zipcode:
            x-oad-type: string
            type: string
          geo:
            x-oad-type: object
            type: object
            properties:
              lat:
                x-oad-type: number
                type: number
                format: float
              lng:
                x-oad-type: number
                type: number
                format: float
            required:
              - lat
              - lng
      phone:
        x-oad-type: string
        type: string
      website:
        x-oad-type: string
        type: string
      company:
        x-oad-type: object
        type: object
        properties:
          name:
            x-oad-type: string
            type: string
          catchPhrase:
            x-oad-type: string
            type: string
          bs:
            x-oad-type: string
            type: string
    required:
      - id
info:
  title: 'JSONPlaceholder API'
  version: v1
  description: 'JSONPlaceholder is an online REST service that you can use whenever you need some fake data. Run this code in a console or from anywhere that CORS and JSONP is supported. It''s like an image placeholder but for web developers and is used for tutorials, faking a server, sharing code examples and more. JSONPlaceholder is a simple fake REST API for testing and prototyping that is powered by JSON Server.'
  contact:
    name: Typicode
    url: 'https://www.programmableweb.com/company/typicode'
  license:
    name: MIT
    url: 'https://github.com/typicode/jsonplaceholder/blob/master/LICENSE'
externalDocs:
  url: 'https://www.programmableweb.com/api/jsonplaceholder'
host: jsonplaceholder.typicode.com
basePath: /
schemes:
  - https
produces:
  - application/json
swagger: '2.0'
