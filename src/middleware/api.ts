import { Middleware, MiddlewareAPI, Dispatch, AnyAction, Action } from "redux";

// Example API middleware implemenation
// Based on the Redux GitHub one

const API_ROOT = 'https://jsonplaceholder.typicode.com/'

// Fetches an API response and normalizes the result JSON according to schema.
// This makes every API response have the same shape, regardless of how nested it was.
function CallApi(endpoint: string, schema: any) {
	const fullUrl = (endpoint.indexOf(API_ROOT) === -1) ? API_ROOT + endpoint : endpoint

	return fetch(fullUrl)
		.then(response =>
			response.json().then(json => {
				if (!response.ok) {
					return Promise.reject(json)
				}

				return json();
			})
		)
}

// Action key that carries API call info interpreted by this Redux middleware.
export const CALL_API = 'call_api'

interface TestAction extends Action {
	effect<T>(action: T): void
}

const MyApiMiddleware: Middleware = (api: MiddlewareAPI<any>) => (next: Dispatch<TestAction>) => ((action: TestAction) => {
	if (action.effect instanceof Function) action.effect(action)

	console.log(`MIDDLEWARE: ${action.type}`);
	
	return next(action)
}) as Dispatch<TestAction>

export default MyApiMiddleware;