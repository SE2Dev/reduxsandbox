import React, { Component } from 'react';
import './App.css';

import { TestPage } from './pages/TestPage'
import Layout from './components/Layout';
import { Route } from 'react-router';
import { DefaultPage } from './pages/DefaultPage';
import FetchData from './components/FetchData';

export class App extends Component {
	render() {
		return (
			<Layout>
				<Route exact path='/' component={DefaultPage} />
				<Route path='/test' component={TestPage} />
				<Route path='/fetchdata/:startIndex?' component={FetchData} />
			</Layout>
		);
	}
}

export default App;
