import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from '../store/Todos';
import { AutoTable } from '../utililty/AutoTable';
import { ApplicationState } from '../store/configureStore'

interface IFetchDataProps {
	requestTodos: (startIndex: number) => void;
	match: {
		params: {
			startIndex: string;
		}
	};
	todos: {};
}

interface IFetchDataState {
	todos: {
		startIndex: number;
	}
}

class FetchData extends Component<IFetchDataProps, IFetchDataState> {
	componentWillMount() {
		// This method runs when the component is first added to the page
		const startIndex = parseInt(this.props.match.params.startIndex, 10) || 0;
		this.props.requestTodos(startIndex);
	}

	componentWillReceiveProps(nextProps: any) {
		// This method runs when incoming props (e.g., route params) change
		const startIndex = parseInt(nextProps.match.params.startIndex, 10) || 0;
		this.props.requestTodos(startIndex);
	}

	render() {
		return (
			<div>
				<h1>Todo List</h1>
				<p>This component demonstrates fetching data from the server and working with URL parameters.</p>
				{renderTodosTable(this.props)}
				{renderPagination(this.props)}
			</div>
		);
	}
}

function renderTodosTable(props: any) {
	return (
		<AutoTable data={props.todos} />
	);
}

function renderPagination(props: any) {
	const prevStartIndex = (props.startIndex || 0) - 1;
	const nextStartIndex = (props.startIndex || 0) + 1;

	return <p className='clearfix text-center'>
		<Link className='btn btn-default pull-left' to={`/fetchdata/${prevStartIndex}`}>Previous</Link>
		<Link className='btn btn-default pull-right' to={`/fetchdata/${nextStartIndex}`}>Next</Link>
		{props.isLoading ? <span>Loading...</span> : []}
	</p>;
}

function mapStateToProps(state: ApplicationState) {
	return {
		todos: state.todos_state
	} as IFetchDataProps;
}

function mapDispatchToProps(dispatch: any) {
	return bindActionCreators(actionCreators, dispatch);
}

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(FetchData);
