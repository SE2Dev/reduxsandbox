import React from 'react';
import { Col, Grid, Row } from 'react-bootstrap';
import NavMenu from './NavMenu';

export default class Layout extends React.Component {
	render() {
		return (
			<Grid fluid>
				<Row>
					{/* The site-wide nav menu */}
					<Col sm={3}>
						<NavMenu />
					</Col>

					{/* The actual page content */}
					<Col sm={9}>
						{this.props.children}
					</Col>
				</Row>
			</Grid>
		);
	}
}