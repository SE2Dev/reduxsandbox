import React from 'react';
import { Link } from 'react-router-dom';
import { Glyphicon, Nav, Navbar, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import './NavMenu.css';

export default class NavMenu extends React.Component {
	render() {
		const header = (
			<Navbar.Header>
				<Navbar.Brand>
					<Link to={'/'}>API Sandbox</Link>
				</Navbar.Brand>
				<Navbar.Toggle />
			</Navbar.Header>
		);

		const collapse = (
			<Navbar.Collapse>
				<Nav>
					<LinkContainer to={'/'} exact>
						<NavItem>
							<Glyphicon glyph='home' /> Home
							</NavItem>
					</LinkContainer>
					<LinkContainer to={'/test'}>
						<NavItem>
							<Glyphicon glyph='education' /> Test
							</NavItem>
					</LinkContainer>
					<LinkContainer to={'/fetchdata'}>
						<NavItem>
							<Glyphicon glyph='th-list' /> Fetch data
							</NavItem>
					</LinkContainer>
				</Nav>
			</Navbar.Collapse>
		);

		return (
			<Navbar inverse fixedTop fluid collapseOnSelect>
				{header}
				{collapse}
			</Navbar>
		);
	}
}
