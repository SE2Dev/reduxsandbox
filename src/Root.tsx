import React from 'react';
import { Provider } from 'react-redux';

import { configureStore } from './store/configureStore';
import { createBrowserHistory, History } from 'history';
import { ConnectedRouter } from 'connected-react-router';

// Create browser history to use in the Redux store
const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href')!;
const history = createBrowserHistory({ basename: baseUrl });

// Get the application-wide store instance, prepopulating with state from the server where available.
const initialState = (window as any).__INITIAL_STATE__;
const store = configureStore(history, initialState);

/**
 * Interface that describes the props structure 
 * of the Root component. These must be passed
 * when creating a Root component - in this particular
 * case, it's recommended to initialize these in an
 * enclosed scope to prevent polluting the global scope.
 *
 * @export
 * @interface IRootProps
 */
export interface IRootProps {
}

/**
 * Interface that describes the state structure 
 * of the Root component.
 * 
 * This is also the interface 
 * that we're using for our app-wide store for redux.
 *
 * @export
 * @interface IRootState
 */
export interface IRootState {
}

/**
 * The top level component for our entire program.
 * 
 * This handles top level middlewares such as the following:
 * + Provider - Top-level store provider for Redux
 * + ConnectedRouter - Top-level routing and history handler for React-Router + Redux
 *
 * @export
 * @class Root
 * @extends {Component}
 */
export class Root extends React.Component<IRootProps, IRootState>  {
	public render() {
		return (
			<Provider store={store}>
				<ConnectedRouter history={history}>
					{this.props.children}
				</ConnectedRouter>
			</Provider>
		);
	}
}

export default Root;
