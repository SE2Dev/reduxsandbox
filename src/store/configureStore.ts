import { applyMiddleware, combineReducers, compose, createStore, ReducersMapObject } from 'redux';
import thunk from 'redux-thunk';

import api from '../middleware/api'
import * as Todos from './Todos'

/* import * as WeatherForecasts from './WeatherForecasts'; */

import { Middleware, StoreEnhancer } from 'redux';

// This needs to be explicitly imported due to conflicting type names with Typescript's standard library
import { History } from "history";
import { routerMiddleware, connectRouter } from 'connected-react-router';

/**
 * The top-level state object
 *
 * @export
 * @interface ApplicationState
 */
export interface ApplicationState {
	/* 	readonly counter: ReturnType<typeof Counter.reducer>;
		readonly weatherForecasts: ReturnType<typeof WeatherForecasts.reducer>; */

	readonly todos_state: ReturnType<typeof Todos.reducer>;
}

/**
 * Configure the app-wide store
 *
 * @export
 * @template S ApplicationState interface
 * @param {History} history
 * @param {S} [initialState] Preloaded application state
 * @returns Instantiated app-wide store
 */
export function configureStore<S>(history: History, initialState?: S) {
	// Specify the top-level reducers we're using
	// Whenever an action is dispatched, Redux will update each top-level application state property using
	// the reducer with the matching name. It's important that the names match exactly, and that the reducer
	// acts on the corresponding ApplicationState property type.
	const reducers = {
		/* counter: Counter.reducer,
		weatherForecasts: WeatherForecasts.reducer */
		todos_reducer: Todos.reducer,
	} as ReducersMapObject;

	// Specify the top-level middlewares we're using
	const middleware: Middleware[] = [
		thunk as Middleware,		// Used for thunks presumably
		api,						// Add our custom API Middleware
		routerMiddleware(history)	// For dispatching history actions
	];

	// In development, use the browser's Redux dev tools extension if installed
	const enhancers = [];
	const isDevelopment = process.env.NODE_ENV === 'development';
	if (isDevelopment && typeof window !== 'undefined' && (window as any).devToolsExtension) {
		enhancers.push((window as any).devToolsExtension());
	}

	// Generate the top-level reducer by combining the map of
	// our specified reducers with one for the middleware reducers
	const rootReducer = combineReducers({
		...reducers,
		router: connectRouter(history)
	});

	// Compose a new enhancer function that combines all of the required enhancers
	// so we can pass it to CreateStore
	const composedEnhancers = compose(
		applyMiddleware(...middleware) as StoreEnhancer<S>,
		...enhancers);

	// Instantiate the app-wide store instance
	return createStore(
		rootReducer,		// reducer
		initialState,		// preloadedState
		composedEnhancers	// enhancer
	);
}
