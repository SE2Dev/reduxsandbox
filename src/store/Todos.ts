import { Reducer, ActionCreator } from 'redux';

const API_ROOT = 'https://jsonplaceholder.typicode.com/'

const requestTodosType = 'REQUEST_TODOS';
const receiveTodosType = 'RECEIVE_TODOS';

const initialState = { todos: [], isLoading: false };

export const actionCreators = {
	// TODO figure these out
	requestTodos: (startIndex: number) => async (dispatch: any, getState: any) => {
		console.log(getState());
		if (startIndex === getState().todos_reducer.startIndex) {
			// Don't issue a duplicate request (we already have or are loading the requested data)
			return;
		}

		dispatch({ type: requestTodosType, startIndex });

		const url = `todos`; // /${startIndex}`;
		const response = await fetch(`${API_ROOT}${url}`);
		const todos = await response.json();

		dispatch({ type: receiveTodosType, startIndex, todos });
	}
};

// Magic async reducer
export const reducer: Reducer<any> = (state, action) => {
	state = state || initialState;

	if (action.type === requestTodosType) {
		return {
			...state,
			startIndex: action.startIndex,
			isLoading: true
		};
	}

	if (action.type === receiveTodosType) {
		return {
			...state,
			startIndex: action.startIndex,
			todos: action.todos,
			isLoading: false
		};
	}

	return state;
};