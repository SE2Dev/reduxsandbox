import React, { Component } from 'react'
import { connect } from 'react-redux';

import {AutoTable } from '../utililty/AutoTable'

const API_ROOT = 'https://jsonplaceholder.typicode.com/'

// Fetches an API response and normalizes the result JSON according to schema.
// This makes every API response have the same shape, regardless of how nested it was.
function CallApi(endpoint: string) {
	const fullUrl = (endpoint.indexOf(API_ROOT) === -1) ? API_ROOT + endpoint : endpoint

	return fetch(fullUrl)
		.then(response =>
			response.json().then(json => {
				if (!response.ok) {
					return Promise.reject(json)
				}

				return json;
			})
		)
}

interface ITestPageState {
	todos: []
}

export class TestPage extends Component<{}, ITestPageState> {
	constructor(props: any) {
		super(props);

		this.state = {
			todos: []
		};
	}

	componentDidMount() {
		CallApi("todos").then((todos) => {
			this.setState({
				todos
			});
		});
	}

	render() {
		return (
			<div>
				<p>Data</p>
				<AutoTable data={this.state.todos}></AutoTable>
			</div>
		);
	}
}
